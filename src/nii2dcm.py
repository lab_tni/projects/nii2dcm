#%% imports
import pydicom as pydi
import pydicom._storage_sopclass_uids as storage_sopclass
import SimpleITK as sitk
import numpy as np
import pandas as pd
import nibabel as nib
import tempfile

from os.path import join as opj
import uuid
import time
import os
import sys
import glob
import json
import argparse
from pkg_resources import require

import multiprocessing
from functools import partial


#%% function definitions
def convert2abs(path):
    if os.path.isabs(path):
        return path
    else:
        return os.path.normpath(opj(os.getcwd(), path))

def person_names_callback(dataset, data_element):
    if data_element.VR == "PN":
        data_element.value = "anonymous"

def curves_callback(dataset, data_element):
    if data_element.tag.group & 0xFF00 == 0x5000:
        del dataset[data_element.tag]


def delete_tags(dcm, annonymize):
    """
    Delete unwanted tags from our sample dicom header
    Fields we don't want to copy
     Rescale intercept, Rescale slope
     Modification time
     modification date
     Image type
     Image orientation
     Slice thickness
     window center
     window width
     SOP instance UID
     PixelData
     Image position patient
     Instance number
     Operator name (retired field)
     SeriesInstanceUID
     StudyIntstanceUID
     Acquisition matrix
     Bits Stored
     High Bit
     Frame of reference
    :param dcm: the pydicom structure
    :return: modified structure
    """

    ignore = [("0x0028", "0x1052"), ("0x0028", "0x1053"),
              ("0x0008", "0x0031"), ("0x0008", "0x0021"),
              ("0x0008", "0x0008"), ("0x0020", "0x0037"),
              ("0x0018", "0x0050"), ("0x0028", "0x1050"),
              ("0x0028", "0x1050"), ("0x0008", "0x0018"),
              ("0x0008", "0x0016"), ("0x7fe0", "0x0010"),
              ("0x0020", "0x0032"), ("0x0020", "0x0013"),
              ("0x0008", "0x1070"), ("0x0020", "0x000e"),
              ("0x0020", "0x000d"), ("0x0018", "0x1310"),
              ("0x0028", "0x0101"), ("0x0028", "0x0102"),
              ("0x0020", "0x0052")]
    for i in ignore:
        tg = pydi.tag.Tag(i)
        if tg in dcm:
            del dcm[tg]

    # annonymize
    if annonymize:
        #t1d = annonymize_header(t1d)
        toRemove = [("0x0008", "0x0020"),
        ("0x0008", "0x0021"),
        ("0x0008", "0x0022"),
        ("0x0008", "0x0023"),
        #("0x0008", "0x0080"), 
        #("0x0008", "0x0081"),
        #("0x0008", "0x0090"), 
        #("0x0008", "0x1010"),
        #("0x0008", "0x1030"), 
        #("0x0008", "0x1050"),
        ("0x0010", "0x0010"), 
        ("0x0010", "0x0020"),
        ("0x0010", "0x0030"),
        ("0x0010", "0x0040"),
        ("0x0008","0x0012")]
        for i in toRemove:
            tg = pydi.tag.Tag(i)
            if tg in dcm:
                dcm[tg].value = "anonymous"

        dcm.walk(person_names_callback)
        dcm.walk(curves_callback)

        
    #dcm.remove_private_tags() # instead of below ? ..
    # Can we delete all the private tags
    #private_keys = [k for k in dcm.keys() if k.is_private]
    #for k in private_keys:
    #    del dcm[k]
    return dcm

def check_isotropy(sitkImage):
    """Return the dimension number which will return a slice with
       isotropic voxels. If the image is isotropic in 3 dimensions,
       return the dimension leading to the smallest number of slices.
       If the image has isotropic axial slices, and the data is
       layed out in MNI space, this function will return 2,
       because sitkImage[:,:, 5] would return an isotropic
       slice and the "5" corresponds to image dimension 2.
       Now rounding to 6 decimal places before selecting the
       plane to use. Rounding is only used in plane selection.
       Original dimensions used in writing dicoms.
       """
    spacing = sitkImage.GetSpacing()
    #nibable header.get_zooms()
    sparray = np.array(spacing)
    # round to 6 decimal 
    spu = np.unique(np.around(sparray, 6))
    if np.unique(spu).ravel().shape[0] == 3:
        message='No plane with isotropic voxels - stopping - {},{},{}'.format(sparray[0], sparray[1], sparray[2])
        raise ValueError(message)

    sparray = np.around(sparray, 6)
    sb = sparray == sparray[0]
    if np.all(sb):
        dimensions = sitkImage.GetSize()
        # nibable header.get_data_shape()
        oddoneout = dimensions.index(min(dimensions))
    else:
        if sb.sum() == 2:
            sb = np.logical_not(sb)
        # find the index of True
        oddoneout = np.nonzero(sb)[0].item(0)
    return oddoneout

def get_direction(sitkImage, planeidx):
    k = np.array(sitkImage.GetDirection())
    # nibable image.affine ?
    # nib.as_closest_canonical(img)
    #print("planeidx",planeidx)
    #invert sign for correct display
    k = k.reshape([3, 3])
    #k[(slice(None),[0,1,2])] *= -1
    
    # plane selection will come into account
    idxs = [0, 1, 2]
    idxs.remove(planeidx)
    idxtuple = (slice(None), idxs)
    direction = list(-k[idxtuple].transpose().ravel())
    #print("resulting direction", direction)
    return direction

def str2ds(strings):
    """
    Convert numbers to strings conforming to dicom rules,
    maximum 16 characters.
    :param strings:
    :return: formatted string representation.
    """
    # Decimal string encoding, maximum 16 characters
    h = ['{:+16.8e}'.format(x) for x in strings]
    return h

def dcm_uuid():
    thisuuid = uuid.uuid4()
    uid = '2.25.' + str(thisuuid.int)
    return uid

def scale_slice(imslice, outrange=None):
    """
    Scale a slice for storing as UInt16 dicom
    :param imslice: floating point, 2D simpleITK image
    :param outrange: desired output range, default based on pixel type
    :return: dict containing the following fields
       rescaled_image: simpleitk image with scaling
       origmax: maximum of input
       origmin: minimum of input
       rescaleslope: (origmax - origmin)/outrange
       rescaleintercept: origmin (redundant)
    """

    mm = sitk.MinimumMaximumImageFilter()
    # nibable/numpy equivalent ?
    mm.Execute(imslice)
    mx = mm.GetMaximum()
    mn = mm.GetMinimum()
    if outrange is None or outrange == 0:
        outrange = float(pow(2, 16) - 1)
    imrange = float(mx - mn)
    rescaleslope = imrange / outrange
    rescaleintercept = mn

    # newslice = (imslice - rescaleintercept)/rescaleslope
    newslice = sitk.ShiftScale(imslice, rescaleintercept,
                               1.0 / rescaleslope)
    newslice = sitk.Cast(newslice, sitk.sitkUInt16)
    # both with numpy ops

    return {'rescaled_image': newslice, 'origmax': mx, 'origmin': mn,
            'rescaleslope': rescaleslope,
            'rescaleintercept': rescaleintercept}

def mk_indexing_tuple(index, plane):
    if plane == 2:
        return((slice(None), slice(None), index))
    if plane == 1:
        return((slice(None), index, slice(None)))
    if plane == 0:
        return((index, slice(None), slice(None)))

    raise ValueError("Invalid plane number")

#from https://github.com/DevelopmentalImagingMCRI/karawun/blob/master/karawun/karawun.py
def sitk_nifti_to_dicom(niftifile, dcmprefix, outdir, dicomfile=None,
                        Description=None, StudyUID=None, PatientID = None, FrameUID=None,
                        SeriesNum=None, annonymize=False, seriesnumber=1):
    """:param niftifile: (string) path to nifti image
    :param dicomfile: (string) path to a dicom file - used to supply some
                   important tags
    :param dcmprefix: (string) prefix for output filenames.
                  _slicenum.dcm will be appended
    :param outdir: (string) target directory, will be created
                if doesn't exist.
    :param Description: (string) to populate the SeriesDescription field.
    :param StudyUID: (string) Study Instance UID - will generate one
                          if none.
    :param FrameUID: (string) a UID that should be common for all
                 dicoms in a common space
    :param SeriesNum: (int) unique within a study - helps identify
                  files in a series
    :return: dictionary of list of strings containing InstanceUIDs for
        constructing other dicoms, the dicom
        structure containing the common parts, and nifti details
        for matching up with label images.
     Notes :
     Scaling is an issue. Lots of dicom conversion tools don't
     apply scaling, because they aren't
     part of the MRImage standard. Need to decide what
     to do with this.
     Brainlab doesn't cope with dicoms with anisotropic voxels
     within slice. Dicoms we acquire are always isotropic within
     slice, so attempt to figure out automatically which planes to
     write.
     Here is the selection, if in MNI orientation to start with.
     Axial planes : nif[:,:,i], direction = list(k[:,0:2].transpose().ravel())
     Sagittal planes: nif[i,:,:]
     coronal planes: nif[:,i,:]
    """

    # Read in as float and rescale to UInt16, with rescale values
    print("reorienting with nibable... ")
    img = nib.load(niftifile)
    reor = nib.as_closest_canonical(img)
    #img_ar = reor.get_fdata()
    #new_img = sitk.GetImageFromArray(img_ar)
    
    f = tempfile.NamedTemporaryFile()
    tmpname = f.name+".nii.gz"
    nib.save(reor, tmpname)
    new_img = sitk.ReadImage(niftifile, sitk.sitkFloat32)
    #nif = sitk.ReadImage(tmpname, sitk.sitkFloat32)
    f.close()

    new_img = sitk.Flip(new_img, [False, False, True])

    writer = sitk.ImageFileWriter()
    writer.KeepOriginalImageUIDOn()

    os.makedirs(outdir, exist_ok=True)

    spacing = new_img.GetSpacing()

    scalednif_dict = scale_slice(new_img)
    new_img_scaled = scalednif_dict['rescaled_image']
    gmx = scalednif_dict['origmax']
    gmn = scalednif_dict['origmin']
    # print('pixelsize: max:',int(gmx),"min:", int(gmn))

    RescaleIntercept = scalednif_dict['rescaleintercept'] 
    RescaleInterceptDS = str2ds([RescaleIntercept])[0]
    RescaleSlope = scalednif_dict['rescaleslope']
    RescaleSlopeDS = str2ds([RescaleSlope])[0]

    print(new_img_scaled.GetSize())

    if StudyUID is None:
        StudyInstanceUID = dcm_uuid()
    else:
        StudyInstanceUID = StudyUID

    if PatientID is not None:
        PatientID = PatientID
        PatientName = PatientID
    else: 
        PatientID = ''
        PatientName = 'none^none'
        
    if Description is not None:
        SeriesDescription = Description
        ProtocolName = Description
    else:
        SeriesDescription = "unknown_series"
    # Create the series instance UID
    SeriesInstanceUID = dcm_uuid()

    def writeSlices(series_tag_values, new_img, i):
        image_slice = new_img[:,:,i]
        imagenumber = i+1 # new_img.GetDepth() - i # alternative = i + 1
        # Tags shared by the series.
        list(map(lambda tag_value: image_slice.SetMetaData(tag_value[0], tag_value[1]), series_tag_values))

        # Slice specific tags.
        image_slice.SetMetaData("0008|0012", time.strftime("%Y%m%d")) # Instance Creation Date
        image_slice.SetMetaData("0008|0013", time.strftime("%H%M%S")) # Instance Creation Time

        # Setting the type to CT preserves the slice location.
        image_slice.SetMetaData("0008|0060", "MR")  # set the type to CT so the thickness is carried over
        location  = new_img.TransformIndexToPhysicalPoint((0,0,i)) 
        # (0020, 0032) image position patient determines the 3D spacing between slices.
        image_slice.SetMetaData("0020|0032", '\\'.join(map(str,location))) # Image Position (Patient)
        image_slice.SetMetaData("0020|0013", str(imagenumber)) # Instance Number
        image_slice.SetMetaData("0020|0012", str(seriesnumber)) # Acquisition Number

        image_slice.SetMetaData("0018|5100", 'HFS') # Patient Position
        image_slice.SetMetaData("0020|1041", str(spacing[2]*imagenumber)) # Slice Location

        # image_slice.SetMetaData('0028|1053', RescaleSlopeDS) # rescale slope
        # image_slice.SetMetaData('0028|1052', RescaleInterceptDS)  # rescale intercept
        # image_slice.SetMetaData('0028|0100', '16')  # bits allocated
        # image_slice.SetMetaData('0028|0101', '16')  # bits stored
        # image_slice.SetMetaData('0028|0102', '15')  # high bit
        # image_slice.SetMetaData('0028|0103', '1') # pixel representation
        # image_slice.SetMetaData('0028|1054', 'US') #RescaleType
        # mx = gmx
        # mn = gmn
        # image_slice.SetMetaData('0028|0106', int(mn)) #SmallestImagePixelValue
        # image_slice.SetMetaData('0028|0107' , int(mx)) #LargestImagePixelValue

        image_slice.SetMetaData('0010|0020', PatientID)
        image_slice.SetMetaData('0010|0010', PatientName)
        image_slice.SetMetaData('0020|000E', SeriesInstanceUID)

        image_slice.SetMetaData('0010|0030', '19930307' ) # PatientBirthDate
        image_slice.SetMetaData('0010|0040', 'O') # PatientSex
        image_slice.SetMetaData('0020|0010', '1') # StudyID
        image_slice.SetMetaData('0020|0011', str(seriesnumber)) # SeriesNumber
        image_slice.SetMetaData('0018|0088',str(spacing[2])) # spacing between slices

        # Write to the output directory and add the extension dcm, to force writing in DICOM format.
        writer.SetFileName(os.path.join(outdir,dcmprefix + "-" + format(imagenumber, "04") + ".dcm"))
        writer.Execute(image_slice)

    modification_time = time.strftime("%H%M%S")
    modification_date = time.strftime("%Y%m%d")
    direction = new_img.GetDirection()
    
    if dicomfile is not None:
        dcm = pydi.dcmread(dicomfile,stop_before_pixels=True, specific_tags=["SeriesDescription"])
        SeriesDescription = dcm.SeriesDescription
        
    series_tag_values = [("0008|0031",modification_time), # Series Time
                  ('0020|000D', StudyInstanceUID),
                  #("0008|0018",StudyInstanceUID), # 
                  ("0008|0030",modification_time), # Study Time
                  ("0008|0021",modification_date), # Series Date
                  ("0008|0008","DERIVED\\SECONDARY"), # Image Type
                  #("0020|000e", "1.2.826.0.1.3680043.2.1125."+modification_date+".1"+modification_time), # Series Instance UID
                  ("0020|0037", '\\'.join(map(str, (direction[0], direction[3], direction[6],# Image Orientation (Patient)
                                                    direction[1],direction[4],direction[7])))),
                  ("0008|103e", SeriesDescription)] # Series Description

    list(map(lambda i: writeSlices(series_tag_values, new_img_scaled, i), range(new_img_scaled.GetDepth())))

    return 

def write_to_file(dcmSlice, sliceIndex, dcmprefix, index, outdir ):
    fname = dcmprefix + "_" + format(sliceIndex, "04") + ".dcm"
    fname = os.path.join(outdir, fname)
    pydi.filewriter.dcmwrite(fname, dcmSlice,
                                 write_like_original=False)

def find_dicom_paths(subject, bids_sourcedata_path):
    desc2pathDict = {}

    dicom_files = glob.glob(bids_sourcedata_path+'/*')
    id_names = [x for y in ['osepa_id', 'lab_id','neurorad_id','folder_id'] if y in subject.keys() for x in subject[y]]

    for f in dicom_files:
        name = os.path.basename(f)
        # print("Searching", name, "for", id_names)
        if name in id_names:
            # print("Found DICOM File", f)
            #dir name is not just series description but may contain _NUMBER at the end
            ret_path = opj(f,'raw_data')
            norm_path = os.path.normpath(ret_path)
            for r in glob.glob(norm_path + '/*'):
                dcm_file = glob.glob(r+'/*')[0]
                with pydi.dcmread(dcm_file,stop_before_pixels=True, specific_tags=["SeriesDescription"]) as ds:
                    desc2pathDict[ds.SeriesDescription] = dcm_file

    return desc2pathDict

def convert_nii2dcm(subject, bids_dir, bids_source_path, out_base_dir, annonymize=False):
    subject_path = opj(bids_dir, subject.participant_id, 'anat')
    files = glob.glob(opj(subject_path,'*.nii.gz'))

    out_sub_dir = opj(out_base_dir, subject.participant_id)

    dicom_paths = find_dicom_paths(subject, bids_source_path)
    seriesnumber = 1
    for f in files:
        filename = os.path.basename(f)
        json_filename  = filename.split('.')[0]+'.json'
        with open(opj(subject_path,json_filename), 'r') as jf:
            sidecar_data = json.load(jf)
        series_description = sidecar_data['SeriesDescription']
        # replace spaces with underscores
        series_description_filename = series_description.replace(" ","_")
        try:
            dicom_path = dicom_paths[series_description]
        except KeyError:
            print("Series Description",series_description,"not found")
        
        # print(series_description)
        # print(dicom_paths)
        #split the dcm path twice to get the original folder name
        folder_path = os.path.split(os.path.split(dicom_path)[0])[1]
        out_dir = os.path.normpath(opj(out_sub_dir, folder_path))

        if len(glob.glob(out_dir+'/*')) > 0:
            #folder not empty -> skip
            # print('Dicoms exist in: ',out_dir)
            continue
        dicom_prefix = filename.split('.')[0].split('_')[-1] #MODALITY name
        if dicom_path != '':
            print('Converting',f,'...')
            print("Params used: \n dicom_path:",dicom_path,\
                "\n dicom_prefix:", dicom_prefix,\
                "\n out_dir:", out_dir, \
                "\n Participant ID:", subject.participant_id, \
                "\n Patient ID:", subject.study_id)
            sitk_nifti_to_dicom(f, dicom_prefix, out_dir, dicomfile=dicom_path, PatientID=subject.study_id, annonymize=annonymize, seriesnumber=seriesnumber) 
        else:
            print('f',f)
            # print('dicom_path',dicom_path)
            # print('dicom_file',dicom_path)
            # print('dicom_prefix', dicom_prefix)
            # print( 'out_dir',out_dir)
            # print('subject.participant_id',subject.participant_id)
        seriesnumber += 1
 
def preproc_ids(df):
    if 'osepa_id' in df.columns:
        df.osepa_id = df.osepa_id.apply(lambda x: [t.strip() for t in str(x).split(',')] if x is not np.nan else [])
    if 'lab_id' in df.columns:
        df.lab_id = df.lab_id.apply(lambda x: [t.strip() for t in str(x).split(',')] if x is not np.nan else [])
    if 'neurorad_id' in df.columns:
        df.neurorad_id = df.neurorad_id.apply(lambda x: [t.strip() for t in str(x).split(',')] if x is not np.nan else [])
    if 'folder_id' in df.columns:
        df.folder_id = df.folder_id.apply(lambda x: [t.strip() for t in str(x).split(',')] if x is not np.nan else [])
    return df

def convert_single_nifti(in_file, out_folder):
    filename = os.path.basename(in_file)
    inFilePath = os.path.split(in_file)[0]
    json_filename  = filename.split('.')[0]+'.json'
    if os.path.isfile(json_filename):
        with open(opj(inFilePath,json_filename), 'r') as jf:
            sidecar_data = json.load(jf)
            series_description = sidecar_data['SeriesDescription']
    else:
        series_description = os.path.basename(in_file).split('.')[0]

    print("Converting single file...")
    print("Infile:", in_file)
    print("Series Description:", series_description)
    print("Outfolder:", out_folder)
    
    sitk_nifti_to_dicom(in_file, series_description, out_folder)


def main():
    """Let's go"""

    """Load arguments for main"""
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=""" Convert niftis back to DICOMs and possibly remove identifying information from the header nii2dcm {}""".format(require("nii2dcm")[0].version #TODO
        ), epilog=""" Documentation not yet at https://gitlab.com/lab_tni/projects/nii2dcm """)

    parser.add_argument('--version', action='version',
                        help='Display verison.', version=require("nii2dcm")[0].version)

    parser.add_argument(
        "-n", "--nifti", required=False, help="Convert single nifti file."
    )

    parser.add_argument(
        "-b", "--bids_path", required=False, help="BIDS conform dir containing all relevant niftis. participants.tsv needs to allow a mapping between the files in sourcedata and the participant_ids."
    )

    parser.add_argument(
        "-p","--participants_file", required=False, help="provide non standart path to participants.tsv file"
    )

    parser.add_argument("-i", "--id", required=False, help="Single participant ID") #TODO

    parser.add_argument(
        "-s", "--source_path", required=False, default='sourcedata', help="none standard sourcedata path relative to bids_path. default is bids_dir/sourcedata"
    )

    parser.add_argument(
        "-o",
        "--output_path",
        required=False,
        default='sourcedata_nii2dcm',
        help="output_path relative to bids dir, default is sourcedata_nii2dcm",
    )

    parser.add_argument(
        "-a",
        "--anonymize",
        action="store_true",
        help="""
        delete identifying information from dicom header""",
    )

    parser.add_argument(
        "-m",
        "--multiproc",
        action="store_true",
        help="""
        control whether multi- or singlecore processing should be used""",
    )

    if len(sys.argv) == 1:
        parser.print_help()
        return 0

    try:
        args = parser.parse_args()
    except argparse.ArgumentError:
            parser.print_usage()
            exit(-1)

    if args.nifti:
        if args.output_path:
            outfolder = args.output_path
        else:
            outfolder = '.'
        convert_single_nifti(args.nifti,outfolder )
        print('Finished.')
        return 0

    bids_dir_ = convert2abs(args.bids_path)

    if args.participants_file:
        participants_file = convert2abs(args.participants_file)
    else:
        participants_file = opj(bids_dir_, 'participants.tsv')

    bids_source_path_ = convert2abs(args.source_path)
    out_base_dir_ = convert2abs(args.output_path)
    os.makedirs(out_base_dir_,exist_ok=True)

    df = pd.read_csv(participants_file, sep="\t",dtype=object)

    df = preproc_ids(df)

    if args.id:
        row = df.loc[df['participant_id'] == args.id]
        if len(row) == 0:
            print("Subject ID not found!")
            return
        if len(row) > 1:
            print("Found too many subjects for that ID!")
            return
        for i, r in row.iterrows():
            convert_nii2dcm(r, bids_dir=bids_dir_, bids_source_path= bids_source_path_, out_base_dir=out_base_dir_,annonymize=args.anonymize)

    else:
        if args.multiproc:
            print('Using ',multiprocessing.cpu_count(),'cores...')
            with multiprocessing.Pool(multiprocessing.cpu_count()) as p:
                p.map(partial(convert_nii2dcm, bids_dir=bids_dir_, bids_source_path= bids_source_path_, out_base_dir=out_base_dir_, annonymize=args.anonymize), [r for i,r in df.iterrows()])
        else:
            print('Using single core...')
            for i, r in df.iterrows():
                convert_nii2dcm(r, bids_dir=bids_dir_, bids_source_path= bids_source_path_, out_base_dir=out_base_dir_,annonymize=args.anonymize)
    print("Finished.")

#%%
if __name__ == "__main__":
    sys.exit(main())
#%%