# nii2dcm

Convert niftis back to DICOM

---
INSTALL

```python setup.py install```

---
USAGE

```nii2dcm -b path_to_bids_dataset [options]```

---
OPTIONS

```--help``` display help

```--bids_path```, ```-b``` BIDS conform dir containing all relevant niftis. participants.tsv needs to allow a mapping between the files in sourcedata and the participant_ids.

```--participant_id [id]```, ```-i [id]``` (currently not working) single participant ID

```--source_path [path]```, ```-s [path]``` default: 'sourcedata'. none standard sourcedata path relative to bids_path. default is bids_dir/sourcedata

```--output_path [path]```, ```-o [path]``` default: 'sourcedata_nii2dcm'. output_path relative to bids dir, default is sourcedata_nii2dcm

```--anonymize```, ```-a``` delete identifying information from dicom header

```--multiproc```, ```-m``` control whether multi- or singlecore processing should be used

---

code adopted from https://github.com/DevelopmentalImagingMCRI/karawun
