#!/usr/bin/env python
# -*- coding: utf-8 -*-
# type: ignore
# pylint: disable=exec-used

import os
from setuptools import setup, find_packages

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

# def load_version():
#     """Execute nii2dcm.version in a global dictionary"""
#     global_dict = {}
#     with open(os.path.join("nii2dcm", "version.py")) as _:
#         exec(_.read(), global_dict)
#     return global_dict


# def install_requires():
#     """Get list of required modules"""
#     required = []
#     for module, meta in _VERSION["REQUIRED_MODULE_METADATA"]:
#         required.append("{}>={}".format(module, meta["min_version"]))
#     return required


#_VERSION = load_version()
DISTNAME = "nii2dcm"
VERSION = "0.9.1" 
ENTRY_POINTS = {
"console_scripts": [
        "nii2dcm = src.nii2dcm:main",
    ],
}
AUTHOR = "Lennart Walger"
AUTHOR_EMAIL = "lennart.walger@ukbonn.de"
DESCRIPTION = (
        "everything around handling lab data and making it bids compatible"
)
with open("README.md", encoding="utf-8") as _:
    LONG_DESCRIPTION = _.read()
LICENSE = "GPLv3+"
PROJECT_URLS = {
    "Documentation": "https://gitlab.com/lab_tni/projects/nii2dcm",
    "Source Code": "https://gitlab.com/lab_tni/projects/nii2dcm",
}
CLASSIFIERS = [
    "Intended Audience :: Healthcare Industry",
    "Intended Audience :: Science/Research",
    "Operating System :: MacOS",
    "Operating System :: Microsoft :: Windows",
    "Operating System :: Unix",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3.6",
    "Programming Language :: Python :: 3.7",
    "Programming Language :: Python :: 3.8",
    "Programming Language :: Python :: 3.9",
    "Topic :: Scientific/Engineering",
    "Topic :: Scientific/Engineering :: Bio-Informatics",
    "Topic :: Scientific/Engineering :: Medical Science Apps.",
]


if __name__ == "__main__":
    setup(
        name=DISTNAME,
        version=VERSION,
        packages=find_packages(exclude=['data', 'figures', 'output', 'notebooks', 'build']),
        entry_points=ENTRY_POINTS,
        python_requires=">=3.7",
        use_scm_version=True,
        setup_requires=['setuptools_scm'],
        install_requires=[
          'future>=0.17.1',
          'pydicom',
          'SimpleITK',
          'numpy',
          'pandas',
          ],
        include_package_data=True,
        author=AUTHOR,
        author_email=AUTHOR_EMAIL,
        description=DESCRIPTION,
        long_description=LONG_DESCRIPTION,
        long_description_content_type="text/markdown",
        # keywords="",
        license=LICENSE,
        project_urls=PROJECT_URLS,
        classifiers=CLASSIFIERS,
    )